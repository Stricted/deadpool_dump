#!/vendor/bin/sh
if ! applypatch --check EMMC:/dev/block/recovery$(getprop ro.boot.slot_suffix):25165824:b85ded927e12f278b04e72fd2cfeb3179faced42; then
  applypatch  \
          --patch /vendor/recovery-from-boot.p \
          --source EMMC:/dev/block/boot$(getprop ro.boot.slot_suffix):16777216:e44f34c058cf628e5b1d57b302a60a4be0d41f9d \
          --target EMMC:/dev/block/recovery$(getprop ro.boot.slot_suffix):25165824:b85ded927e12f278b04e72fd2cfeb3179faced42 && \
      log -t recovery "Installing new recovery image: succeeded" || \
      log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
